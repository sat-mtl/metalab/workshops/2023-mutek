# 2023 Mutek workshop - Deploying ephemeral immersive spaces

This repository will contain materials for the Mutek workshop [Deploying ephemeral immersive spaces](https://forum.mutek.org/en/shows/2023/workshop-deploying-ephemeral-immersive-spaces)

## Schedule

* 35 min - Présentation/Présentation:
  * 05 min - [Presentation SAT/Metalab (demo reel)](https://vimeo.com/349368754)
  * 15 min - Présentation des outils: 
    * 5 min - [LivePose video](https://vimeo.com/604196712)
    * 5 min - [Splash video](https://vimeo.com/268028595)
    * 5 min - [Puara](https://github.com/Puara/). ([demo video](https://youtu.be/Eb19StiQhu0))
    * 5 min - SATIE : [real-time demo](satie_demo.scd)
  * 10 min - Cas d'étude : OSM
    * l'histoire du projet et l'objectifs du projet de l'OSM
* 50 min - Hands-on!    
  * 30 min - Installation d'espace immersif ([connection example](connection_example.png))
    * 10 min - video mapping callibration
    * 10 min - configuration de la spatialisation du son
    * 10 min - envoyer du contenu sur le réseau
  * 10 min - navigation sur l'espace interactif
      * avec mobile et télécommande
      * Connexion de sources OSC externes
  * 10 min - real-time video content à Splash et SATIE
* 5 min - Q/A et remarques finales